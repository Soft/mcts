// Quick and dirty single-player tic-tac-toe implementation

extern crate mcts_rs;
extern crate rand;

use std::fmt::{self, Formatter, Display};
use std::str::FromStr;
use std::io::{BufReader, BufRead, Lines, Stdin, stdin};
use rand::ThreadRng;

use mcts_rs::{State, UCT, RandomSimulation, MaxChild, Combine, ValueView, search};

#[derive(Debug,PartialEq,Eq,Copy,Clone)]
enum Piece {
    X, O
}

#[derive(Debug,PartialEq,Eq,Copy,Clone)]
enum Winner {
    Piece(Piece),
    Tie
}

#[derive(Clone, Debug)]
struct TicTacToe {
    turn: Piece,
    board: [[Option<Piece>; 3]; 3]
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
struct Move(usize, usize);

impl Move {
    fn new(x: usize, y: usize) -> Result<Move, &'static str> {
        if x <= 2 && y <= 2 {
            Ok(Move(x, y))
        } else {
            Err("Coordinate out of bounds")
        }
    }
}

impl Default for TicTacToe {
    fn default() -> Self {
        let nones = [None, None, None];
        TicTacToe {
            turn: Piece::X,
            board: [nones, nones, nones]
        }
    }
}

type Line = (Option<Piece>, Option<Piece>, Option<Piece>);

impl TicTacToe {
    fn available_spots<'a>(&'a self) -> Box<Iterator<Item=Move> + 'a> {
        let iter = self.board.iter()
            .enumerate()
            .flat_map(|(i, r)| r.iter()
                      .enumerate()
                      .filter(|&(_, p)| p.is_none())
                      .map(move |(n, _)| Move(i, n)));
        Box::new(iter) as Box<Iterator<Item=Move> + 'a>
    }

    fn rows<'a>(&'a self) -> Box<Iterator<Item=Line> +'a> {
        let row = move |n: usize| (self.board[n][0], self.board[n][1], self.board[n][2]);
        let iter = (0..3).map(row);
        Box::new(iter)
    }
}

impl State for TicTacToe {
    type Transition = Move;
    type Agent = Piece;
    type Outcome = Winner;

    fn winner(&self) -> Option<Self::Outcome> {
        fn matching(a: Option<Piece>, b: Option<Piece>, c: Option<Piece>)
                    -> Option<Piece> {
            match (a, b, c) {
                (Some(Piece::X), Some(Piece::X), Some(Piece::X)) => Some(Piece::X),
                (Some(Piece::O), Some(Piece::O), Some(Piece::O)) => Some(Piece::O),
                _ => None
            }
        }
        let row = |n: usize| matching(self.board[n][0], self.board[n][1], self.board[n][2]);
        let col = |n: usize| matching(self.board[0][n], self.board[1][n], self.board[2][n]);
        let diag1 = matching(self.board[0][0], self.board[1][1], self.board[2][2]);
        let diag2 = matching(self.board[2][0], self.board[1][1], self.board[0][2]);
        let rows = (0..3).map(row).fold(None, |a, b| a.or(b));
        let cols = (0..3).map(col).fold(None, |a, b| a.or(b));
        match rows.or(cols).or(diag1).or(diag2) {
            None => {
                // If we haven't found a winner and there
                // are no moves left to do the game is a tie
                if self.available_spots().next().is_none() {
                    Some(Winner::Tie)
                } else {
                    None
                }
            },
            Some(piece) => Some(Winner::Piece(piece))
        }
    }

    fn advance(&mut self, &Move(x, y): &Self::Transition) {
        self.board[x][y] = Some(self.turn);
        self.turn = next_piece(self.turn);
    }

    fn transitions<'a>(&'a self) -> Box<Iterator<Item=Self::Transition> + 'a> {
        if self.winner().is_some() {
            return Box::new(std::iter::empty::<Self::Transition>());
        } else {
            return self.available_spots();
        }
    }

    fn active_agent(&self) -> Self::Agent {
        self.turn
    }
}

fn next_piece(piece: Piece) -> Piece {
    match piece {
        Piece::X => Piece::O,
        Piece::O => Piece::X
    }
}

impl FromStr for Move {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut components = s.split_whitespace();
        let x = components.next()
            .ok_or("Failed to get X")?
            .parse()
            .map_err(|_| "X is not a valid coordinate")?;
        let y = components.next()
            .ok_or("Failed to get Y")?
            .parse()
            .map_err(|_| "Y is not a valid coordinate")?;
        Move::new(x, y)
    }
}

impl Display for TicTacToe {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        fn to_str(p: Option<Piece>) -> &'static str {
            match p {
                None => " ",
                Some(Piece::X) => "X",
                Some(Piece::O) => "O"
            }
        }
        write!(f, "Turn: {:?}\n", self.turn)?;
        write!(f, "-------\n")?;
        for (a, b, c) in self.rows() {
            write!(f, "|{}|{}|{}|\n", to_str(a), to_str(b), to_str(c))?
        }
        write!(f, "-------\n")
    }
}

fn main() {
    fn read_move(mut lines: &mut Lines<BufReader<Stdin>>, board: &TicTacToe) -> Move {
        loop {
            match lines.next()
                .expect("End of input")
                .expect("Failed to read line")
                .parse() {
                    Ok(m) => {
                        if board.available_spots()
                            .find(|n| *n == m)
                            .is_some() {
                                return m
                            } else {
                                println!("The position has already been played");
                                continue;
                            }
                    },
                    Err(_) => {
                        println!("Invalid move");
                        continue;
                    }
                }
        }
    }
    let mut board = <TicTacToe as Default>::default();
    let mut lines = BufReader::new(stdin()).lines();

    let selector: UCT = Default::default();
    let mut simulator: RandomSimulation<ThreadRng> = Default::default();
    let add = Combine::<_, f64, _>::new(|var, outcome| {
        *var += match outcome {
            &Winner::Piece(Piece::O) => 1.0,
            &Winner::Piece(Piece::X) => -1.0,
            &Winner::Tie => 0.0
        }
    });

    while !board.terminal() {
        println!("{}", board);
        let move_ = match board.turn {
            Piece::X => read_move(&mut lines, &board),
            Piece::O => search(board.clone(),
                               &selector,
                               &mut simulator,
                               10000,
                               &add,
                               &ValueView,
                               MaxChild)
                .unwrap()
        };
        board.advance(&move_);
    }

    println!("{}", board);
    println!("Winner: {:?}", board.winner());
}
