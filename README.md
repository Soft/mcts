# Monte Carlo Tree Search for Rust

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

Experimental Monte Carlo Tree Search library for Rust programming language. The
aim of the crate is to be generic enough to support wide variety of different
use cases while avoiding paying for things you do not use.

## TODO

- [ ] More validation for core algorithms
- [ ] Root parallelization
- [ ] Parallel simulations
- [ ] Additional selection policies
- [ ] Weighted transitions

