//! Lower-level interface to searching functionality, most users should likely
//! use [Search](struct.Search.html) instead.

use super::*;

/// Run one round of the search process
pub fn step<S, V, A, B, U, E, O>(root: Node<S, V>,
                                 selector: &A,
                                 mut simulator: &mut B,
                                 updater: &U,
                                 view: &E)
    where A: Selector<S, V, E, O> + ?Sized,
          B: Simulator<S> + ?Sized,
          S: State,
          U: Updater<V, S>,
          E: View<V, S, O>,
          V: Default {

    // # Select
    let node = root.traverse(selector, view);

    // # Expand
    // What is the correct action to take here?
    // What if the selected node does not have any unexpanded transitions left?
    // Should we just return or run the simulation with the selected node itself?
    let node = match node.expand() {
        Some(node) => node,
        _ => node
    };

    // # Simulate
    let outcome = node.clone().simulate(simulator);

    // # Backpropagate
    node.backpropagate(outcome, updater);
}

/// Build a search tree
pub fn build_tree<S, V, A, B, L, U, E, O>(state: S,
                                          selector: &A,
                                          mut simulator: &mut B,
                                          limit: L,
                                          updater: &U,
                                          view: &E)
                                          -> Node<S, V>
    where S: State,
          V: Default,
          A: Selector<S, V, E, O> + ?Sized,
          B: Simulator<S> + ?Sized,
          L: Into<Limit>,
          U: Updater<V, S>,
          E: View<V, S, O> {
    let limit: Limit = limit.into();
    let root = Node::new_root(state);
    for _ in limit.iter() {
        step(root.clone(), selector, simulator, updater, view);
    }
    root
}

pub fn search<S, V, A, B, L, U, G, E, O>(state: S,
                                         selector: &A,
                                         mut simulator: &mut B,
                                         limit: L,
                                         updater: &U,
                                         view: &E,
                                         transition_selector: G)
                                         -> Option<S::Transition>
    where S: State,
          V: Default,
          A: Selector<S, V, E, O> + ?Sized,
          B: Simulator<S> + ?Sized,
          L: Into<Limit>,
          U: Updater<V, S>,
          E: View<V, S, O>,
          G: TransitionSelector<S, V, E, O> {
    let tree = build_tree(state, selector, simulator, limit, updater, view);
    let children = tree.children();
    transition_selector.select(&children, view)
}
