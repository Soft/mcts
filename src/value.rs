use std::marker::PhantomData;
use std::ops::Add;

use state::State;
use node::Node;

/// Updaters are used for combining new simulation outcomes into nodes' values.
pub trait Updater<V, S>
    where S: State {

    fn update(&self, node: Node<S, V>, outcome: &S::Outcome);
}

impl<F, V, S> Updater<V, S> for F
    where S: State, F: Fn(Node<S, V>, &S::Outcome) {
    fn update(&self, node: Node<S, V>, outcome: &S::Outcome) {
        self(node, outcome)
    }
}

/// Views are used for converting nodes' values into something selectors can consume.
pub trait View<V, S, O>
    where S: State {
    fn view(&self, node: Node<S, V>) -> O;
}

impl<F, V, S, O> View<V, S, O> for F
    where S: State, F: Fn(Node<S, V>) -> O {
    fn view(&self, node: Node<S, V>) -> O {
        self(node)
    }
}

pub struct ValueView;

impl<V, S> View<V, S, V> for ValueView
    where S: State, V: Clone {
    fn view(&self, node: Node<S, V>) -> V {
        node.value().clone()
    }
}

pub struct Average<S, F>
    where S: State, F: Fn(&S::Outcome) -> f64 {
    value: F,
    _marker: PhantomData<S> // I am not sure why this seems to be necessary
}

impl<S, F> Average<S, F>
    where S: State, F: Fn(&S::Outcome) -> f64 {
    pub fn new(func: F) -> Average<S, F> {
        Average {
            value: func,
            _marker: PhantomData
        }
    }
}

impl<S, F> Updater<f64, S> for Average<S, F>
    where S: State, F: Fn(&S::Outcome) -> f64 {

    fn update(&self, node: Node<S, f64>, outcome: &S::Outcome) {
        let new = (self.value)(outcome);
        let old = *node.value();
        let visits = node.visits() as f64;
        *node.value_mut() = (old * visits + new) / (visits + 1.0);
    }
}

pub struct Combine<S, V, F>
    where S: State, F: Fn(&mut V, &S::Outcome) {
    value: F,
    _marker1: PhantomData<S>,
    _marker2: PhantomData<V>
}

impl<S, V, F> Combine<S, V, F>
    where S: State, F: Fn(&mut V, &S::Outcome) {
    pub fn new(func: F) -> Combine<S, V, F> {
        Combine {
            value: func,
            _marker1: PhantomData,
            _marker2: PhantomData
        }
    }
}

impl<S, V, F> Updater<V, S> for Combine<S, V, F>
    where S: State, F: Fn(&mut V, &S::Outcome) {

    fn update(&self, node: Node<S, V>, outcome: &S::Outcome) {
        (self.value)(&mut node.value_mut(), outcome);
    }
}

pub struct Adder;

impl<V, S> Updater<V, S> for Adder // Not the most efficient implementation
    where S: State, S::Outcome: Into<V> + Clone, V: Add<Output=V> + Clone {

    fn update(&self, node: Node<S, V>, outcome: &S::Outcome) {
        let value: V = outcome.clone().into();
        let new = node.value().clone() + value;
        *node.value_mut() = new;
    }
}


