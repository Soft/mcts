//! # Monte Carlo Tree Search for Rust
//!
//! This crate aims to provide an extensible framework for applying Monte Carlo
//! tree search in variety of scenarios.
//!
//! # Usage
//! 
//! Users should at the very least implement the [State](trait.State.html)
//! trait. The `State` trait represent the rules specific to a given domain. If
//! a "heavier" playout strategy is desired, this can be achieved by
//! implementing a custom [Simulator](trait.Simulator.html).
//!
//! # Structure of the Library
//!
//! The core of the `mcts` library can be distilled into five components:
//!
//! - [State](trait.State.html): The State trait encapsulates the game logic and
//!   enables `mcts` to build game trees and run simulations.
//! - [Selector](trait.Selector.html): Selectors are used to navigate the game
//!   tree balancing exploration and exploitation.
//! - [Simulator](trait.Simulator.html): Simulators run playouts using game
//!   states. Simulators produce [Outcomes](trait.State.html#associatedtype.Outcome)
//!   that can be incorporated into the game tree using [Updaters](value/trait.Updater.html).
//! - [Values, Views & Updaters](value/index.html): Aside from game state, each
//!   node of the game tree contains a value reflecting the results from
//!   simulations. This value can be anything, but a simple floating point number
//!   or a vector of them should suffice for common applications.
//! - [Transition Selectors](transition_selector/index.html): Once the game tree
//!   has been built to a sufficient degree, the last step of the algorithm is
//!   to select the best
//!   [Transition](trait.State.html#associatedtype.Transition) from the ones
//!   available. This selection is guided by transition selectors.
//! 
//! # Status
//! At present, this crate is still experimental. Consider it a pre-alpha.

extern crate rand;

mod node;
mod state;
pub mod value;
pub mod selector;
pub mod simulator;
pub mod transition_selector;
mod limit;
pub mod search;

pub use state::State;
pub use value::*;
pub use node::*;
pub use selector::*;
pub use simulator::*;
pub use transition_selector::*;
pub use limit::*;
pub use search::search;
