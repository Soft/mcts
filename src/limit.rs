
use std::time::{Duration, SystemTime};
use std::ops::RangeFrom;

/// Limit represents a policy about when the search process be stopped.
pub enum Limit {
    /// Limit search to a certain number of iterations.
    Iterations(u32),
    /// Limit the time spent on the search.
    Duration(Duration)
}

impl From<u32> for Limit {
    fn from(n: u32) -> Self {
        Limit::Iterations(n)
    }
}

impl From<Duration> for Limit {
    fn from(duration: Duration) -> Self {
        Limit::Duration(duration)
    }
}

struct DurationIterator {
    duration: Duration,
    starting_time: Option<SystemTime>,
    iter: RangeFrom<u32>
}

impl DurationIterator {
    fn new(duration:Duration) -> DurationIterator {
        DurationIterator {
            duration: duration,
            starting_time: None,
            iter: (0..)
        }
    }
}

impl Iterator for DurationIterator {
    type Item = u32;

    fn next(&mut self) -> Option<u32> {
        if self.starting_time.is_none() {
            self.starting_time = Some(SystemTime::now());
        }
        let elapsed = self.starting_time.unwrap().elapsed()
            .expect("Failed to get elapsed time");
        if elapsed < self.duration {
            self.iter.next()
        } else {
            None
        }
    }
}

impl Limit {
    /// Returns an iterator that will yield successive integers until the limit is reached.
    pub fn iter(&self) -> Box<Iterator<Item=u32>> {
        match *self {
            Limit::Iterations(n) => Box::new(0..n),
            Limit::Duration(duration) => Box::new(DurationIterator::new(duration))
        }
    }
}

