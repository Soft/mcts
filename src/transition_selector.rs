//!
//! `TransitionSelector<S>`s are used to select the best `Transition` from a
//! finished search tree.
//!

use std::cmp::{PartialOrd, Ordering};

use super::*;

/// `TransitionSelector<S>` represents a policy for selecting the best move from
/// a finished search tree.
pub trait TransitionSelector<S, V, E, O>
    where S: State,
          E: View<V, S, O> {
    /// `select` chooses the best child among the supplied `nodes`.
    fn select(&self, nodes: &[Node<S, V>], view: &E) -> Option<S::Transition>;
}

impl<F, S, V, E, O> TransitionSelector<S, V, E, O> for F
    where F: Fn(&[Node<S, V>], &E) -> Option<S::Transition>,
          S: State,
          E: View<V, S, O> {
    fn select(&self, nodes: &[Node<S, V>], view: &E) -> Option<S::Transition> {
        self(nodes, view)
    }
}

/// `MaxChild` policy selects the child with the maximum value.
pub struct MaxChild;

impl<S, V, E> TransitionSelector<S, V, E, f64> for MaxChild
    where S: State,
          E: View<V, S, f64> {
    fn select(&self, nodes: &[Node<S, V>], view: &E) -> Option<S::Transition> {
        fn value<S: State, V, E: View<V, S, f64>>(node: Node<S, V>, view: &E) -> f64 {
            let visits = node.visits() as f64;
            let value: f64 = view.view(node);
            value / visits
        }
        let node = nodes.iter()
            .max_by(|a, b| value((*a).clone(), view)
                    .partial_cmp(&value((*b).clone(), view))
                    .unwrap_or(Ordering::Equal));
        node.and_then(|n| n.transition())
    }
}

/// `RobustChild` policy selects the child that has been visited the most.
pub struct RobustChild;

impl<S, V, E, O> TransitionSelector<S, V, E, O> for RobustChild
    where S: State,
          E: View<V, S, O> {
    fn select(&self, nodes: &[Node<S, V>], _: &E) -> Option<S::Transition> {
        let node = nodes.iter().max_by_key(|n| n.visits());
        node.and_then(|n| n.transition())
    }
}

/// `SecureChild` policy selects the child that maximizes the lower confidence
/// bound.
pub struct SecureChild(pub f64);

impl<S, V, E> TransitionSelector<S, V, E, f64> for SecureChild
    where S: State,
          E: View<V, S, f64> {
    fn select(&self, nodes: &[Node<S, V>], view: &E) -> Option<S::Transition> {
        let value = |node: &Node<S, V>| {
            let value: f64 = view.view(node.clone());
            (value / node.visits() as f64) + (self.0 / (node.visits() as f64).sqrt())  
        };
        let node = nodes.iter()
            .max_by(|a, b| value(&a)
                    .partial_cmp(&value(&b))
                    .unwrap_or(Ordering::Equal));
        node.and_then(|n| n.transition())
    }
}

