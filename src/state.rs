
/// Trait representing the state of the game world
/// This trait encapsulates the rules of the game. Users wishing to apply Monte
/// Carlo tree search for their problem domain have to implement this trait.
pub trait State: Clone {

    /// Transition represents the kinds of moves that can be applied to this `State`.
    type Transition: Clone;

    /// Agent represents an actor that can act on the `State` by applying `Transition`s to it.
    type Agent: Eq;

    type Outcome;
    
    /// Checks if the state is a terminal state and returns the winner
    fn winner(&self) -> Option<Self::Outcome>;

    /// Checks if the state is a terminal state, that is, if there is a winner
    /// for this state.
    fn terminal(&self) -> bool {
        self.winner().is_some()
    }

    /// Apply transition to the state mutating it.
    fn advance(&mut self, &Self::Transition); // Maybe this should be able to error

    /// Iterator returning legal transitions from the state
    fn transitions<'a>(&'a self) -> Box<Iterator<Item=Self::Transition> + 'a>; // It would be nice to unbox this

    /// Returns the active agent
    fn active_agent(&self) -> Self::Agent;

}
