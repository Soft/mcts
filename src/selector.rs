//! Selectors are used to determine how trees are traversed during Monte Carlo
//! tree search.

use std::cmp::{PartialOrd, Ordering};

use super::*;

/// `Selector<T>` represents a selection policy that can be used to traverse the
/// search tree.
pub trait Selector<T, V, E, O> // Could I move I into the method signature
    where T: State,
          E: View<V, T, O> {

    /// Given a node, `select` chooses one of node's children.
    /// The `parent` node will always have children.
    fn select(&self, parent: Node<T, V>, view: &E) -> Node<T, V>;
}

impl<F, T, V, E, O> Selector<T, V, E, O> for F
    where F: Fn(Node<T, V>, &E) -> Node<T, V>,
          T: State,
          E: View<V, T, O> {
    fn select(&self, parent: Node<T, V>, view: &E) -> Node<T, V> {
        self(parent, view)
    }
}

/// [Upper Confidence Bound 1](https://en.wikipedia.org/wiki/Monte_Carlo_tree_search#Exploration_and_exploitation) applied to trees (UCT) selection policy
pub struct UCT {
    c: f64
}

/// Default `UCT` uses √2 as the exploration parameter
impl Default for UCT {
    fn default() -> UCT {
        UCT::new((2.0 as f64).sqrt())
    }
}

impl UCT {

    /// Create a new `UCT` instance with given exploration parameter
    pub fn new(c: f64) -> UCT {
        UCT {
            c: c
        }
    }
}

impl<T, V, E> Selector<T, V, E, f64> for UCT
    where T: State,
          E: View<V, T, f64> {

    fn select(&self, parent: Node<T, V>, view: &E) -> Node<T, V> {
        let uct = |node: &Node<T, V>| {
            let value: f64 = view.view(node.clone());
            (value / node.visits() as f64) + self.c * ((parent.visits() as f64).ln() / node.visits() as f64).sqrt();   
        };
        parent.children().iter()
            .max_by(|a, b| uct(&a).partial_cmp(&uct(&b))
                    .unwrap_or(Ordering::Equal))
            .map(|n| n.clone())
            .unwrap()
    }

}

