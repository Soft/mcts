//! Simulators are used to run playouts on `State`s.

use rand::{Rng, ThreadRng, thread_rng};

use super::*;

/// Simulation strategy determines how a simulation is performed.
pub trait Simulator<S>
    where S: State {

    /// Run the simulation until a terminal state is found. Returns the `Agent`
    /// that won.
    fn simulate(&mut self, state: &mut S) -> S::Outcome;
}

impl<S, F> Simulator<S> for F
    where F: FnMut(&mut S) -> S::Outcome, S: State {
    fn simulate(&mut self, state: &mut S) -> S::Outcome {
        self(state)
    }
}

/// `RandomSimulation<G>` applies random transitions to a given `state` until a
/// terminal state is reached. This does not depend on any domain specific
/// knowledge about the game except for the rules specified by the `State`
/// trait.
pub struct RandomSimulation<G>
    where G: Rng {
    generator: G
}

/// Default for `RandomSimulation<ThreadRng>` creates a new RanSim with a thread specific
/// random number generator.
impl Default for RandomSimulation<ThreadRng> {
    fn default() -> RandomSimulation<ThreadRng> {
        RandomSimulation::<ThreadRng>::new(thread_rng())
    }
}

impl<G> RandomSimulation<G>
    where G: Rng {
    /// Create a new `RandomSimulation<T>` using a given random number generator.
    pub fn new<T: Rng>(gen: T) -> RandomSimulation<T> {
        RandomSimulation {
            generator: gen
        }
    }
}

impl<S, G> Simulator<S> for RandomSimulation<G>
    where S: State, G: Rng {
    /// `RanSim<G>` applies random moves to the `state` until a terminal state is reached.
    fn simulate(&mut self, state: &mut S) -> S::Outcome {
        loop {
            match state.winner() {
                None => {
                    let transitions: Vec<_> = state.transitions().collect();
                    let selected = self.generator.choose(&transitions).unwrap();
                    state.advance(selected);
                },
                Some(outcome) => return outcome
            }
        }
    }
}

