use std::rc::{Rc, Weak};
use std::cell::{RefCell, Ref, RefMut};

use super::*;

/// Reference counted node for use in search trees.
pub struct Node<T: State, V>(Rc<RefCell<TreeNode<T, V>>>);

/// Cloning a node only increments the reference count associated with the node.
impl<T, V> Clone for Node<T, V>
    where T: State {
    fn clone(&self) -> Node<T, V> {
        Node(self.0.clone())
    }
}

// TODO: Implement Debug trait

type WeakNodeRef<T, V> = Weak<RefCell<TreeNode<T, V>>>;

struct TreeNode<T, V>
    where T: State {

    /// Expanded children
    children: Vec<Node<T, V>>,

    /// Reference to the parent node or None if the node is a root
    parent: Option<WeakNodeRef<T, V>>,

    /// Number of visits
    visits: u32,

    /// Value of the node
    value: V,

    /// Transitions that can possible from this node, but still have not been expanded
    unexpanded_transitions: Vec<T::Transition>, // Maybe this could be an iterator

    transition: Option<T::Transition>,

    state: T
}

impl<T, V> Node<T, V>
    where T: State {

    /// Create a new node
    fn new(state: T, transition: Option<T::Transition>, parent: Option<Node<T, V>>) -> Node<T, V>
        where V: Default {
        let transitions: Vec<_> = state.transitions().collect();
        let node = TreeNode {
            children: vec![],
            parent: parent.map(|r| Rc::downgrade(&r.0)),
            visits: 0,
            value: Default::default(),
            unexpanded_transitions: transitions,
            transition: transition,
            state: state
        };
        Node(Rc::new(RefCell::new(node)))
    }

    /// Creates a new child node
    pub fn new_child(state: T, transition: T::Transition, parent: Node<T, V>) -> Node<T, V>
        where V: Default {
        Node::new(state, Some(transition), Some(parent))
    }

    /// Create a new root node
    pub fn new_root(state: T) -> Node<T, V>
        where V: Default {
        Node::new(state, None, None)
    }
    /// Does the node have unexpanded transitions
    pub fn unexpanded_transitions(&self) -> bool {
        !self.0.borrow().unexpanded_transitions.is_empty()
    }

    /// Does the node represent a terminal state
    pub fn terminal(&self) -> bool {
        self.0.borrow().state.terminal()
    }

    /// Returns node's aggregated win count
    pub fn value(&self) -> Ref<V> {
        Ref::map(self.0.borrow(), |n| &n.value)
    }

    /// Returns node's aggregated win count
    pub fn value_mut(&self) -> RefMut<V> {
        RefMut::map(self.0.borrow_mut(), |n| &mut n.value)
    }

    /// Returns node's aggregated visit count
    pub fn visits(&self) -> u32 {
        self.0.borrow().visits
    }

    /// Returns a dynamically borrowed slice of node's children
    pub fn children(&self) -> Ref<[Node<T, V>]> {
        Ref::map(self.0.borrow(), |n| &*n.children)
    }

    /// Returns a dynamically borrowed mutable slice of node's children
    pub fn children_mut(&self) -> RefMut<[Node<T, V>]> {
        RefMut::map(self.0.borrow_mut(), |n| &mut *n.children)
    }

    /// Returns the transition that was applied to create this node.
    /// In case the node is a root node return `None`.
    pub fn transition(&self) -> Option<T::Transition> { // FIXME: Maybe return a Ref
        self.0.borrow().transition.clone()
    }

    /// Returns an iterator recursively yielding all nodes in the tree.
    pub fn nodes(&self) -> NodeIterator<T, V> {
        NodeIterator::new(self.clone())
    }

    /// Traverses the node using `selector`
    pub fn traverse<A, E, O>(&self, selector: &A, view: &E) -> Node<T, V> // Reconsider this
        where A: Selector<T, V, E, O> + ?Sized,
              E: View<V, T, O> {
        let mut node = self.clone();

        while !node.terminal() {
            if node.unexpanded_transitions() {
                return node;
            } else {
                node = selector.select(node, view);
            }
        }
        node
    }

    /// Expand a node by creating a new child node.
    /// Returns `None` if there are no unexpanded transitions.
    pub fn expand(&self) -> Option<Node<T, V>>
        where V: Default {
        let mut borrow = self.0.borrow_mut();
        let transition = match borrow.unexpanded_transitions.pop() {
            Some(transition) => transition,
            None => return None
        };
        let mut state = borrow.state.clone();
        state.advance(&transition);
        let child = Node::new_child(state, transition, self.clone());
        borrow.children.push(child.clone());
        Some(child)
    }


    /// Run a simulation on node's state and return the winner
    pub fn simulate<A>(&self, mut simulator: &mut A) -> T::Outcome
        where A: Simulator<T> + ?Sized {
        let mut state = self.0.borrow().state.clone();
        simulator.simulate(&mut state)
    }

    /// Backpropagate simulation results to ancestor nodes
    pub fn backpropagate<U>(&self, outcome: T::Outcome, updater: &U)
        where U: Updater<V, T> {
        let mut current = self.clone();
        loop {
            current = {
                updater.update(current.clone(), &outcome);
                let mut borrow = current.0.borrow_mut();
                borrow.visits += 1;
                match borrow.parent {
                    Some(ref node) => Node(node.upgrade().unwrap()),
                    _ => return
                }
            };
        }
    }

}

pub struct NodeIterator<T, V>
    where T: State {
    stack: Vec<Node<T, V>>
}

impl<T, V> NodeIterator<T, V>
    where T: State {
    fn new(root: Node<T, V>) -> NodeIterator<T, V> {
        NodeIterator {
            stack: vec![root]
        }
    }
}

impl<T, V> Iterator for NodeIterator<T, V>
    where T: State {
    type Item = Node<T, V>;

    fn next(&mut self) -> Option<Node<T, V>> {
        self.stack.pop()
            .map(|node| {
                for child in node.children().iter() {
                    self.stack.push(child.clone());
                }
                node
            })
    }
}
